package org.example;

import imgui.ImGui;
import imgui.gl3.ImGuiImplGl3;
import imgui.glfw.ImGuiImplGlfw;
import org.example.entity.Camera;
import org.example.entity.Entity;
import org.example.model.Light;
import org.example.model.Material;
import org.example.model.Terrain;
import org.example.model.TexturedModel;
import org.example.renderEngine.DisplayManager;

import org.example.renderEngine.Loader;
import org.example.renderEngine.Renderer;
import org.example.shaders.StaticShader;
import org.example.toolbox.AssimpModelLoader;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainGameLoop {
    private static boolean showContextMenu = false;
//    private static boolean useBlinnPhong = true;
//    private static boolean useLighting = true;
    private static int lightMode = 0;

    //     static final int TOTAL_FRAMES = 15;
//     static long lastFrameChangeTime = System.currentTimeMillis();
//     static int frameIndex = 0;
// Define rotation parameters
    private static final float RADIUS = 60.0f;
    private static final float SPEED = 0.01f; // radians per second
    private static float ANGLE = 0.0f;
    private static List<Light> lights = new ArrayList<>();


    public static void main(String[] args) {
        Material matteMaterial = new Material(
                new Vector3f(0.2f, 0.2f, 0.2f), // ambient
                new Vector3f(0.5f, 0.5f, 0.5f), // diffuse
                new Vector3f(0.1f, 0.1f, 0.1f), // specular
                15.0f                           // shininess
        );

        Material shinyMaterial = new Material(
                new Vector3f(0.2f, 0.2f, 0.2f), // ambient
                new Vector3f(0.8f, 0.8f, 0.8f), // diffuse
                new Vector3f(1.0f, 1.0f, 1.0f), // specular
                100.0f                           // shininess
        );
        Material greenLightMaterial = new Material(
                new Vector3f(0.0f, 0.0f, 0.0f), // ambient
                new Vector3f(0.0f, 1.0f, 0.0f), // diffuse (green light)
                new Vector3f(0.0f, 0.0f, 0.0f), // specular
                1.0f                           // shininess
        );

        Material redLightMaterial = new Material(
                new Vector3f(0.0f, 0.0f, 0.0f), // ambient
                new Vector3f(1.0f, 0.0f, 0.0f), // diffuse (red light)
                new Vector3f(0.0f, 0.0f, 0.0f), // specular
                1.0f                           // shininess
        );

        Material blueLightMaterial = new Material(
                new Vector3f(0.0f, 0.0f, 0.0f), // ambient
                new Vector3f(0.0f, 0.0f, 1.0f), // diffuse (blue light)
                new Vector3f(0.0f, 0.0f, 0.0f), // specular
                1.0f                           // shininess
        );

        Material yellowLightMaterial = new Material(
                new Vector3f(0.24725f, 0.1995f, 0.0275f), // ambient
                new Vector3f(0.5508f, 0.2118f, 0.066f), // diffuse (yellow light)
                new Vector3f(0.580594f, 0.223257f, 0.0695701f), // specular
                1.0f                           // shininess
        );
        DisplayManager displayManager = new DisplayManager();
        displayManager.createDisplay();
        Loader loader = new Loader();
        StaticShader shader = new StaticShader();
        Renderer renderer = new Renderer(shader, displayManager.getWindow());
        int totalFrames = 15; // Liczba klatek animacji
        long lastFrameChangeTime = System.currentTimeMillis();
        int frameIndex = 0; // Aktualna klatka animacji
        List<Entity> listOfEntities = new ArrayList<>();

        TexturedModel terrainTexturedModel = AssimpModelLoader.loadModelWithTexture("terrain.obj", "forestTexture.jpg", loader);

        Entity terrainEntity = new Entity(terrainTexturedModel, new Vector3f(0, 0, 0), 0, 0, 0, 2,matteMaterial);
        Terrain terrain = new Terrain(0, 0, loader, terrainTexturedModel.getRawModel());

        TexturedModel treeTexturedModel = AssimpModelLoader.loadModelWithTexture("christmas-tree.obj", "tree-texture.jpg", loader);

        List<Vector3f> treePositions = generateTreePositions(terrain, 30, 5.0f);



        treePositions.forEach(treePosition -> listOfEntities.add(new Entity(treeTexturedModel, treePosition, 0, 0, 0, 4,matteMaterial)));


        TexturedModel sphereTexturedModel = AssimpModelLoader.loadModelWithTexture("sphere.obj", "water.jpg", loader);


        Entity shinySphereEntity = new Entity(sphereTexturedModel, new Vector3f(0, 14, 0), 0, 0, 0, 7, shinyMaterial);
        Entity matteSphereEntity = new Entity(sphereTexturedModel, new Vector3f(0, 14, 0), 0, 0, 0, 7, matteMaterial);
        Entity greenLightSphere = new Entity(sphereTexturedModel, new Vector3f(0, 10, 0), 0, 0, 0, 7, greenLightMaterial,true,0);
        Entity redLightSphere = new Entity(sphereTexturedModel, new Vector3f(0, 10, 0), 0, 0, 0, 7, redLightMaterial,true,1);
        Entity blueLightSphere = new Entity(sphereTexturedModel, new Vector3f(0, 10, 0), 0, 0, 0, 7, blueLightMaterial,true,2);
        Entity yellowLightSphere = new Entity(sphereTexturedModel, new Vector3f(0, 10, 0), 0, 0, 0, 7, yellowLightMaterial,true,3);

        listOfEntities.add(shinySphereEntity);
        listOfEntities.add(matteSphereEntity);
        listOfEntities.add(greenLightSphere);
        listOfEntities.add(redLightSphere);
        listOfEntities.add(blueLightSphere);
        listOfEntities.add(yellowLightSphere);

// Define lights
        lights.add(new Light(new Vector3f(0, 20, 0), new Vector3f(0.1f, 0.1f, 0.1f), new Vector3f(0.0f, 6.0f, 0.0f), new Vector3f(10.0f, 10.0f, 10.0f), new Vector3f(1.0f, 0.0f, 0.01f)));
        lights.add(new Light(new Vector3f(0, 20, 0), new Vector3f(0.1f, 0.1f, 0.1f), new Vector3f(6.0f, 0.0f, 0.0f), new Vector3f(10.0f, 10.0f, 10.0f), new Vector3f(1.0f, 0.0f, 0.01f)));
        lights.add(new Light(new Vector3f(0, 20, 0), new Vector3f(0.1f, 0.1f, 0.1f), new Vector3f(0.0f, 0.0f, 6.0f), new Vector3f(10.0f, 10.0f, 10.0f), new Vector3f(1.0f, 0.0f, 0.01f)));
        lights.add(new Light(new Vector3f(0, 20, 0), new Vector3f(0.1f, 0.1f, 0.1f), new Vector3f(6.0f, 6.0f, 0.0f), new Vector3f(10.0f, 10.0f, 10.0f), new Vector3f(1.0f, 0.0f, 0.01f)));


        // Set up the light and material properties
        Light light = new Light(new Vector3f(0, 13, 0), new Vector3f(0.1f, 0.1f, 0.1f), new Vector3f(12.0f, 12.0f, 12.0f), new Vector3f(12.0f, 12.0f, 12.0f), new Vector3f(1.0f, 0.0f, 0.01f));

        Camera camera = new Camera(displayManager.getWindow());

        while (!GLFW.glfwWindowShouldClose(displayManager.getWindow())) {
            handleInput(displayManager);
            camera.move();
            renderer.prepare();

            shader.start();
            shader.loadViewMatrix(camera);
            shader.loadSampler("textureSampler1", 0);
//            shader.loadBoolean("useBlinnPhong", useBlinnPhong);
//            shader.loadBoolean("useLighting", useLighting);
            shader.loadLightMode(lightMode);


            shader.loadCameraPosition(camera.getPosition());
            shader.loadNumLights(lights.size());

            shader.loadLights(lights);

            //shader.loadLight(light);
           // shader.loadMaterial(material);

            // Update sphere position
            ANGLE += SPEED;
            updateSpherePosition(ANGLE, RADIUS, shinySphereEntity,matteSphereEntity,greenLightSphere,redLightSphere,blueLightSphere,yellowLightSphere);

            renderer.render(terrainEntity, shader, terrainTexturedModel.getTexture().getID());
            listOfEntities.forEach(entity -> renderer.render(entity, shader, entity.getTexturedModel().getTexture().getID()));

            displayManager.startImGuiFrame();
            if (showContextMenu) {
                renderContextMenu();
            }
            displayManager.renderImGui();

            shader.stop();
            displayManager.updateDisplay();
        }

        shader.cleanUp();
        loader.cleanUp();
        displayManager.closeDisplay();
    }

    public static float[] getFrameTextureCoords(int frameIndex, int totalFrames) {
        float frameWidth = 1.0f / totalFrames;
        float uStart = frameIndex * frameWidth;
        float uEnd = uStart + frameWidth;
        return new float[]{
                uEnd, 0.0f,
                uEnd, 1.0f,
                uStart, 1.0f,
                uStart, 0.0f
        };
    }
    private static void handleInput(DisplayManager displayManager) {
        if (GLFW.glfwGetKey(displayManager.getWindow(), GLFW.GLFW_KEY_1) == GLFW.GLFW_PRESS) {
            showContextMenu = !showContextMenu;
        }
    }

    private static void renderContextMenu() {
        ImGui.begin("Lighting Options Menu");
        if (ImGui.button("Directional-Light")) {
            lightMode=3;
        }
        if (ImGui.button("Blinn-Phong")) {
            lightMode=2;
        }
        if (ImGui.button("Phong")) {
            lightMode=1;
        }
        if (ImGui.button("No Lighting")) {
            lightMode=0;
        }
        ImGui.end();
    }
    private static void updateSpherePosition(float angle, float radius, Entity shinySphere, Entity matteSphere, Entity greenLightSphere, Entity redLightSphere, Entity blueLightSphere, Entity yellowLightSphere) {
        float x = radius * (float) Math.sin(angle);
        float z = radius * (float) Math.cos(angle);
        float y = 14.0f; // Fixed height

        shinySphere.setPosition(new Vector3f(x, y, z));
        matteSphere.setPosition(new Vector3f(-x, y, -z));

        float lightRadius = radius / 2;

        // Use a different time factor for angular position but maintain initial offset
        float adjustedAngle = angle / 2;
        greenLightSphere.setPosition(new Vector3f(lightRadius * (float) Math.sin(adjustedAngle), y, lightRadius * (float) Math.cos(adjustedAngle)));
        redLightSphere.setPosition(new Vector3f(lightRadius * (float) Math.sin(adjustedAngle + Math.PI / 2), y, lightRadius * (float) Math.cos(adjustedAngle + Math.PI / 2)));
        blueLightSphere.setPosition(new Vector3f(lightRadius * (float) Math.sin(adjustedAngle + Math.PI), y, lightRadius * (float) Math.cos(adjustedAngle + Math.PI)));
        yellowLightSphere.setPosition(new Vector3f(lightRadius * (float) Math.sin(adjustedAngle + 3 * Math.PI / 2), y, lightRadius * (float) Math.cos(adjustedAngle + 3 * Math.PI / 2)));

        // Update light positions
        lights.get(0).setPosition(greenLightSphere.getPosition());
        lights.get(1).setPosition(redLightSphere.getPosition());
        lights.get(2).setPosition(blueLightSphere.getPosition());
        lights.get(3).setPosition(yellowLightSphere.getPosition());
    }
    public static List<Vector3f> generateTreePositions(Terrain terrain, int numberOfTrees, float minDistanceBetweenTrees) {
        List<Vector3f> treePositions = new ArrayList<>();
        Random random = new Random();

        float minX = terrain.getMinX();
        float maxX = terrain.getMaxX();
        float minZ = terrain.getMinZ();
        float maxZ = terrain.getMaxZ();

        for (int i = 0; i < numberOfTrees; i++) {
            boolean validPosition = false;
            Vector3f position = null;
            int attempts = 0;
            int maxAttempts = 100;

            while (!validPosition && attempts < maxAttempts) {
                float x = minX + random.nextFloat() * (maxX - minX);
                float z = minZ + random.nextFloat() * (maxZ - minZ);
                float y = terrain.getHeight(x, z);

                position = new Vector3f(x, y, z);
                System.out.println("Tree Position: " + position);

                validPosition = true;
                for (Vector3f existingTree : treePositions) {
                    if (position.distance(existingTree) < minDistanceBetweenTrees) {
                        validPosition = false;
                        break;
                    }
                }
                attempts++;
            }

            if (validPosition) {
                treePositions.add(position);
            } else {
                System.out.println("Could not place tree " + (i + 1) + " after " + maxAttempts + " attempts.");
            }
        }

        return treePositions;
    }
}