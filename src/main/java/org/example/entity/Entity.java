package org.example.entity;

import org.example.model.Material;
import org.example.model.TexturedModel;
import org.joml.Vector3f;

public class Entity {

    private TexturedModel texturedModel;
    private Vector3f position;
    private float rotX, rotY, rotZ;
    private float scale;
    private Material material;
    private boolean isLightSource=false;
    private int lightIndex;




    public Entity(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale, Material material) {
        this.texturedModel = texturedModel;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
        this.material=material;
    }

    public Entity(TexturedModel texturedModel, Vector3f position, float rotX, float rotY, float rotZ, float scale, Material material, boolean isLightSource,int lightIndex) {
        this.texturedModel = texturedModel;
        this.position = position;
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
        this.scale = scale;
        this.material=material;
        this.isLightSource=isLightSource;
        this.lightIndex=lightIndex;
    }

    public boolean isLightSource() {
        return isLightSource;
    }

    public void setLightSource(boolean lightSource) {
        isLightSource = lightSource;
    }

    public int getLightIndex() {
        return lightIndex;
    }

    public void setLightIndex(int lightIndex) {
        this.lightIndex = lightIndex;
    }

    public void increasePosition(float dx, float dy, float dz) {
        position.add(dx, dy, dz);
    }

    public void increaseRotation(float dx, float dy, float dz) {
        rotX += dx;
        rotY += dy;
        rotZ += dz;
    }

    // Getters and setters
    public TexturedModel getTexturedModel() {
        return texturedModel;
    }

    public void setTexturedModel(TexturedModel texturedModel) {
        this.texturedModel = texturedModel;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public float getRotX() {
        return rotX;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public float getRotY() {
        return rotY;
    }

    public void setRotY(float rotY) {
        this.rotY = rotY;
    }

    public float getRotZ() {
        return rotZ;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }
    public Material getMaterial() {
        return material;
    }
}
