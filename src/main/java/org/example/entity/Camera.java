package org.example.entity;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import static org.lwjgl.glfw.GLFW.GLFW_CURSOR;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_DISABLED;
import static org.lwjgl.glfw.GLFW.GLFW_CURSOR_NORMAL;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_A;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_D;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_R;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_S;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_T;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_W;
import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_RIGHT;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwGetKey;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetInputMode;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;

public class Camera {

    private Vector3f position = new Vector3f(0, 5, 0);
    private float pitch;
    private float yaw;
    private float roll;
    private long window;
    private boolean isMouseGrabbed = true;

    public Camera(long window) {
        this.window = window;
        glfwSetCursorPosCallback(window, new MouseMoveCallback());
        glfwSetMouseButtonCallback(window, new MouseButtonCallback());
        glfwSetKeyCallback(window, new KeyCallback());
        glfwSetScrollCallback(window, new ScrollCallback());
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    public void move() {
        float moveSpeed = 0.5f;

        Vector3f forward = new Vector3f(
                (float) Math.sin(Math.toRadians(yaw)),
                0,
                -(float) Math.cos(Math.toRadians(yaw))
        ).normalize();

        Vector3f right = new Vector3f(
                (float) Math.sin(Math.toRadians(yaw + 90)),
                0,
                -(float) Math.cos(Math.toRadians(yaw + 90))
        ).normalize();

        if (isKeyPressed(GLFW_KEY_W)) {
            position.add(forward.mul(moveSpeed, new Vector3f()));
        }
        if (isKeyPressed(GLFW_KEY_S)) {
            position.sub(forward.mul(moveSpeed, new Vector3f()));
        }
        if (isKeyPressed(GLFW_KEY_A)) {
            position.sub(right.mul(moveSpeed, new Vector3f()));
        }
        if (isKeyPressed(GLFW_KEY_D)) {
            position.add(right.mul(moveSpeed, new Vector3f()));
        }
        if (isKeyPressed(GLFW_KEY_R)) {
            position.y += moveSpeed;
        }
        if (isKeyPressed(GLFW_KEY_T)) {
            position.y -= moveSpeed;
        }
//        System.out.println("Camera Position: " + position);
//        System.out.println("Camera Rotation: " + pitch + ", " + yaw + ", " + roll);
    }

    private boolean isKeyPressed(int key) {
        return glfwGetKey(window, key) == GLFW_PRESS;
    }

    private class MouseMoveCallback extends GLFWCursorPosCallback {
        private double lastX = 640, lastY = 360; // Initial cursor position

        @Override
        public void invoke(long window, double xpos, double ypos) {
            if (isMouseGrabbed) {
                float dx = (float) (xpos - lastX);
                float dy = (float) (ypos - lastY);
                lastX = xpos;
                lastY = ypos;

                float mouseSensitivity = 0.2f;
                yaw += dx * mouseSensitivity;
                pitch += dy * mouseSensitivity; // Invert the pitch direction

                if (pitch < -90) pitch = -90;
                if (pitch > 90) pitch = 90;
            }
        }
    }

    private class MouseButtonCallback extends GLFWMouseButtonCallback {
        @Override
        public void invoke(long window, int button, int action, int mods) {
            if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
                isMouseGrabbed = !isMouseGrabbed;
                glfwSetInputMode(window, GLFW_CURSOR, isMouseGrabbed ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
            }
        }
    }

    private class KeyCallback extends GLFWKeyCallback {
        @Override
        public void invoke(long window, int key, int scancode, int action, int mods) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                glfwSetWindowShouldClose(window, true);
            }
        }
    }

    private class ScrollCallback extends GLFWScrollCallback {
        @Override
        public void invoke(long window, double xoffset, double yoffset) {
            // Optional: Implement zoom functionality if needed
        }
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }
}
