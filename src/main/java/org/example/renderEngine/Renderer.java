package org.example.renderEngine;

import org.example.entity.Entity;
import org.example.model.Material;
import org.example.model.RawModel;
import org.example.model.TexturedModel;
import org.example.shaders.StaticShader;
import org.example.toolbox.Maths;
import org.example.toolbox.OpenGLUtils;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.glfw.GLFW;

public class Renderer {

    private static final float FOV = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 1000;

    private Matrix4f projectionMatrix;
    private long window;

    public Renderer(StaticShader shader, long window) {
        this.window = window;
        createProjectionMatrix();
        shader.start();
        shader.loadProjectionMatrix(projectionMatrix);
        shader.stop();
    }

    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(0.68f, 0.85f, 0.90f, 1.0f);
    }

    public void render(Entity entity, StaticShader shader, int texture1) {
        // Enable blending for transparency
        GL11.glEnable(GL11.GL_BLEND);
        OpenGLUtils.checkForErrors("Enable Blending");
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        OpenGLUtils.checkForErrors("Blend Function");

        // Bind the VAO of the model and enable vertex attribute arrays
        TexturedModel model = entity.getTexturedModel();
        RawModel rawModel = model.getRawModel();
        GL30.glBindVertexArray(rawModel.getVaoId());
        OpenGLUtils.checkForErrors("Bind VAO");

        GL20.glEnableVertexAttribArray(0); // Position attribute
        OpenGLUtils.checkForErrors("Enable Vertex Attrib Array 0");

        GL20.glEnableVertexAttribArray(1); // Texture coordinate attribute
        OpenGLUtils.checkForErrors("Enable Vertex Attrib Array 1");

        GL20.glEnableVertexAttribArray(2); // Normal attribute
        OpenGLUtils.checkForErrors("Enable Vertex Attrib Array 2");

        // Create a transformation matrix and load it into the shader
        Matrix4f transformationMatrix = Maths.createTransformationMatrix(entity.getPosition(),
                entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
        shader.loadTransformationMatrix(transformationMatrix);
        OpenGLUtils.checkForErrors("Load Transformation Matrix");
        //System.out.println("Transformation Matrix: " + transformationMatrix);

        // Load isLightSource property into the shader
        shader.loadIsLightSource(entity.isLightSource() ? 1 : 0);
        shader.loadLightIndex(entity.getLightIndex());

        // Activate the texture unit and bind the texture
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        OpenGLUtils.checkForErrors("Activate Texture Unit 0");
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture1);
        OpenGLUtils.checkForErrors("Bind Texture");


        // Load texture units to the shader if necessary
        shader.loadTextureUnits(0);
        OpenGLUtils.checkForErrors("Load Texture Units");

        // Load blend factor to shader if it uses this for mixing textures or effects
        shader.loadFloat(shader.getUniformLocation("mixRatio"), 0.5f);
        OpenGLUtils.checkForErrors("Load Blend Factor");

        // Load material properties into the shader
        Material material = entity.getMaterial();
        shader.loadMaterial(material);
        OpenGLUtils.checkForErrors("Load Material Properties");

        // Draw the elements
        GL11.glDrawElements(GL11.GL_TRIANGLES, rawModel.getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
        OpenGLUtils.checkForErrors("Draw Elements");

        // Clean up after rendering
        GL11.glDisable(GL11.GL_BLEND);
        OpenGLUtils.checkForErrors("Disable Blending");

        GL20.glDisableVertexAttribArray(0);
        OpenGLUtils.checkForErrors("Disable Vertex Attrib Array 0");

        GL20.glDisableVertexAttribArray(1);
        OpenGLUtils.checkForErrors("Disable Vertex Attrib Array 1");

        GL20.glDisableVertexAttribArray(2);
        OpenGLUtils.checkForErrors("Disable Vertex Attrib Array 2");

        GL30.glBindVertexArray(0);
        OpenGLUtils.checkForErrors("Unbind VAO");
    }

    private void createProjectionMatrix() {
        int[] width = new int[1];
        int[] height = new int[1];
        GLFW.glfwGetFramebufferSize(window, width, height);
        float aspectRatio = (float) width[0] / height[0];
        float yScale = (float) (1f / Math.tan(Math.toRadians(FOV / 2f)));
        float xScale = yScale / aspectRatio;
        float frustumLength = FAR_PLANE - NEAR_PLANE;

        projectionMatrix = new Matrix4f();
        projectionMatrix.m00(xScale);
        projectionMatrix.m11(yScale);
        projectionMatrix.m22(-((FAR_PLANE + NEAR_PLANE) / frustumLength));
        projectionMatrix.m23(-1);
        projectionMatrix.m32(-((2 * NEAR_PLANE * FAR_PLANE) / frustumLength));
        projectionMatrix.m33(0);
    }
}