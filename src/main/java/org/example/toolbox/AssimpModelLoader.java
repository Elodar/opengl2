package org.example.toolbox;

import org.example.model.RawModel;
import org.example.model.TexturedModel;
import org.example.renderEngine.Loader;
import org.example.texture.ModelTexture;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AINode;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.Assimp;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssimpModelLoader {

    public static RawModel loadModel(String filePath, Loader loader) {
        AIScene aiScene = Assimp.aiImportFile(filePath, Assimp.aiProcess_Triangulate | Assimp.aiProcess_FlipUVs);
        if (aiScene == null) {
            throw new RuntimeException("Failed to load model: " + Assimp.aiGetErrorString());
        }

        AINode rootNode = aiScene.mRootNode();
        AIMesh mesh = AIMesh.create(aiScene.mMeshes().get(0)); // Assuming only one mesh for simplicity

        List<Float> vertices = new ArrayList<>();
        List<Float> normals = new ArrayList<>();
        List<Float> texCoords = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();

        for (int i = 0; i < mesh.mNumVertices(); i++) {
            vertices.add(mesh.mVertices().get(i).x());
            vertices.add(mesh.mVertices().get(i).y());
            vertices.add(mesh.mVertices().get(i).z());

            if (mesh.mNormals() != null) {
                normals.add(mesh.mNormals().get(i).x());
                normals.add(mesh.mNormals().get(i).y());
                normals.add(mesh.mNormals().get(i).z());
            }

            if (mesh.mTextureCoords(0) != null) {
                texCoords.add(mesh.mTextureCoords(0).get(i).x());
                texCoords.add(mesh.mTextureCoords(0).get(i).y());
            } else {
                texCoords.add(0.0f);
                texCoords.add(0.0f);
            }
        }

        for (int i = 0; i < mesh.mNumFaces(); i++) {
            IntBuffer indicesBuffer = mesh.mFaces().get(i).mIndices();
            while (indicesBuffer.remaining() > 0) {
                indices.add(indicesBuffer.get());
            }
        }

        float[] verticesArray = toArray(vertices);
        float[] normalsArray = toArray(normals);
        float[] texCoordsArray = toArray(texCoords);
        int[] indicesArray = indices.stream().mapToInt(i -> i).toArray();

        return loader.loadToVAO(verticesArray, texCoordsArray, indicesArray,normalsArray);
    }

    public static TexturedModel loadModelWithTexture(String fileName, String textureFileName, Loader loader) {
        AIScene aiScene = Assimp.aiImportFile("src/main/resources/" + fileName, Assimp.aiProcess_Triangulate | Assimp.aiProcess_FlipUVs);
        if (aiScene == null) {
            throw new RuntimeException("Failed to load model: " + Assimp.aiGetErrorString());
        }

        AINode rootNode = aiScene.mRootNode();
        AIMesh mesh = AIMesh.create(aiScene.mMeshes().get(0)); // Assuming only one mesh for simplicity

        List<Float> vertices = new ArrayList<>();
        List<Float> normals = new ArrayList<>();
        List<Float> texCoords = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();

        for (int i = 0; i < mesh.mNumVertices(); i++) {
            vertices.add(mesh.mVertices().get(i).x());
            vertices.add(mesh.mVertices().get(i).y());
            vertices.add(mesh.mVertices().get(i).z());

            if (mesh.mNormals() != null) {
                normals.add(mesh.mNormals().get(i).x());
                normals.add(mesh.mNormals().get(i).y());
                normals.add(mesh.mNormals().get(i).z());
            }

            if (mesh.mTextureCoords(0) != null) {
                texCoords.add(mesh.mTextureCoords(0).get(i).x());
                texCoords.add(mesh.mTextureCoords(0).get(i).y());
            } else {
                texCoords.add(0.0f);
                texCoords.add(0.0f);
            }
        }

        for (int i = 0; i < mesh.mNumFaces(); i++) {
            IntBuffer indicesBuffer = mesh.mFaces().get(i).mIndices();
            while (indicesBuffer.remaining() > 0) {
                indices.add(indicesBuffer.get());
            }
        }

        float[] verticesArray = toArray(vertices);
        float[] normalsArray = toArray(normals);
        float[] texCoordsArray = toArray(texCoords);
        int[] indicesArray = indices.stream().mapToInt(i -> i).toArray();
//        System.out.println(Arrays.toString(normalsArray));
        RawModel model =  loader.loadToVAO(verticesArray, texCoordsArray, indicesArray,normalsArray);
        model.setVertices(verticesArray);
        int textureId = loader.loadTexture(textureFileName);
        ModelTexture modelTexture = new ModelTexture(textureId);
        TexturedModel texturedModel = new TexturedModel(model,modelTexture);
        return texturedModel;
    }

    private static float[] toArray(List<Float> list) {
        float[] array = new float[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }
}
