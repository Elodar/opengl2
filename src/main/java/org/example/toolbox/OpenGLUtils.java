package org.example.toolbox;

import org.lwjgl.opengl.GL11;

public class OpenGLUtils {

    public static void checkForErrors(String context) {
        int errorCode;
        while ((errorCode = GL11.glGetError()) != GL11.GL_NO_ERROR) {
            String errorString = getErrorString(errorCode);
            System.err.println("OpenGL Error (" + errorCode + "): " + errorString + " in " + context);
        }
    }

    private static String getErrorString(int errorCode) {
        switch (errorCode) {
            case GL11.GL_NO_ERROR: return "No error";
            case GL11.GL_INVALID_ENUM: return "Invalid enum";
            case GL11.GL_INVALID_VALUE: return "Invalid value";
            case GL11.GL_INVALID_OPERATION: return "Invalid operation";
            case GL11.GL_STACK_OVERFLOW: return "Stack overflow";
            case GL11.GL_STACK_UNDERFLOW: return "Stack underflow";
            case GL11.GL_OUT_OF_MEMORY: return "Out of memory";
            default: return "Unknown error";
        }
    }
}