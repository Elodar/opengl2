#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 textureCoordinates;
layout(location = 2) in vec3 normal;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec3 cameraPos;

uniform int numLights;
uniform vec3 lightPosition[4];
uniform vec3 lightAmbient[4];
uniform vec3 lightDiffuse[4];
uniform vec3 lightSpecular[4];
uniform vec3 lightAttenuation[4];

uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float shininess;
uniform int lightMode;
uniform int isLightSource;
uniform int lightIndex;

out vec3 fragPos;
out vec2 pass_textureCoordinates;
out vec3 fragNormal;
flat out int vLightMode;
flat out int vIsLightSource;
flat out int vLightIndex;

void main() {
    vec4 worldPosition = transformationMatrix * vec4(position, 1.0);
    fragPos = worldPosition.xyz;
    fragNormal = normalize(mat3(transpose(inverse(transformationMatrix))) * normal);
    pass_textureCoordinates = textureCoordinates;

    vLightMode = lightMode;
    vIsLightSource = isLightSource;
    vLightIndex = lightIndex;

    gl_Position = projectionMatrix * viewMatrix * worldPosition;
}