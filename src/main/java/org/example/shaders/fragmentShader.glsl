#version 330 core

in vec3 fragPos;
in vec2 pass_textureCoordinates;
in vec3 fragNormal;
flat in int vLightMode;
flat in int vIsLightSource;
flat in int vLightIndex;

out vec4 outColor;

uniform sampler2D textureSampler;
uniform int numLights;
uniform vec3 lightPosition[4];
uniform vec3 lightAmbient[4];
uniform vec3 lightDiffuse[4];
uniform vec3 lightSpecular[4];
uniform vec3 lightAttenuation[4];

uniform vec3 cameraPos;
uniform vec3 materialAmbient;
uniform vec3 materialDiffuse;
uniform vec3 materialSpecular;
uniform float shininess;

vec3 calculatePhong(vec3 position, vec3 normal, vec3 cameraPos, vec3 lightPosition, vec3 lightAmbient, vec3 lightDiffuse, vec3 lightSpecular, vec3 lightAttenuation, vec3 materialAmbient, vec3 materialDiffuse, vec3 materialSpecular, float shininess) {
    vec3 ambient = lightAmbient * materialAmbient;
    vec3 lightDir = normalize(lightPosition - position);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = lightDiffuse * (diff * materialDiffuse);
    vec3 viewDir = normalize(cameraPos - position);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), shininess);
    vec3 specular = lightSpecular * (spec * materialSpecular);
    float distance = length(lightPosition - position);
    float attenuation = 1.0 / (lightAttenuation.x + lightAttenuation.y * distance + lightAttenuation.z * (distance * distance));
    return (ambient + diffuse + specular) * attenuation;
}

vec3 calculateBlinnPhong(vec3 position, vec3 normal, vec3 cameraPos, vec3 lightPosition, vec3 lightAmbient, vec3 lightDiffuse, vec3 lightSpecular, vec3 lightAttenuation, vec3 materialAmbient, vec3 materialDiffuse, vec3 materialSpecular, float shininess) {
    vec3 ambient = lightAmbient * materialAmbient;
    vec3 lightDir = normalize(lightPosition - position);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = lightDiffuse * (diff * materialDiffuse);
    vec3 viewDir = normalize(cameraPos - position);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), shininess);
    vec3 specular = lightSpecular * (spec * materialSpecular);
    float distance = length(lightPosition - position);
    float attenuation = 1.0 / (lightAttenuation.x + lightAttenuation.y * distance + lightAttenuation.z * (distance * distance));
    return (ambient + diffuse + specular) * attenuation;
}

vec3 calculateDirectionalLight(vec3 normal, vec3 viewDir, vec3 lightDir, vec3 lightAmbient, vec3 lightDiffuse, vec3 lightSpecular, vec3 materialAmbient, vec3 materialDiffuse, vec3 materialSpecular, float shininess) {
    vec3 ambient = lightAmbient * materialAmbient;
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = lightDiffuse * (diff * materialDiffuse);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), shininess);
    vec3 specular = lightSpecular * (spec * materialSpecular);
    return ambient + diffuse + specular;
}

void main() {
    vec4 texColor = texture(textureSampler, pass_textureCoordinates);

    vec3 finalColor = texColor.rgb;

    if (vLightMode == 0) {
        if (vIsLightSource == 1) {
            // Use the light's diffuse color instead of the texture color
            finalColor = mix(texColor.rgb, lightDiffuse[vLightIndex], 0.5);
        }
        outColor = vec4(finalColor, texColor.a); // No lighting
    } else {
        if (vIsLightSource == 1) {
            // Use the corresponding light's diffuse color instead of the texture color
            finalColor = lightDiffuse[vLightIndex];
            outColor = vec4(finalColor, 1.0); // Emissive color
        } else {
            vec3 lightCoef = vec3(0.0);
            if (vLightMode == 1) {
                for (int i = 0; i < numLights; i++) {
                    lightCoef += calculatePhong(fragPos, fragNormal, cameraPos, lightPosition[i], lightAmbient[i], lightDiffuse[i], lightSpecular[i], lightAttenuation[i], materialAmbient, materialDiffuse, materialSpecular, shininess);
                }
            } else if (vLightMode == 2) {
                for (int i = 0; i < numLights; i++) {
                    lightCoef += calculateBlinnPhong(fragPos, fragNormal, cameraPos, lightPosition[i], lightAmbient[i], lightDiffuse[i], lightSpecular[i], lightAttenuation[i], materialAmbient, materialDiffuse, materialSpecular, shininess);
                }
            } else if (vLightMode == 3) {
                vec3 viewDir = normalize(cameraPos - fragPos);
                vec3 lightDir = normalize(vec3(0.0, 10.0, 0.0)); // Static directional light from above
                vec3 lightAmbient = vec3(1.1, 1.1, 1.1); // Ambient color of directional light
                vec3 lightDiffuse = vec3(3.0, 2.0, 0.0); // Diffuse color of directional light
                vec3 lightSpecular = vec3(1.0, 1.0, 1.0); // Specular color of directional light
                lightCoef = calculateDirectionalLight(fragNormal, viewDir, lightDir, lightAmbient, lightDiffuse, lightSpecular, materialAmbient, materialDiffuse, materialSpecular, shininess);
            }
            finalColor = texColor.rgb * lightCoef;
            outColor = vec4(finalColor, texColor.a);
        }
    }
}