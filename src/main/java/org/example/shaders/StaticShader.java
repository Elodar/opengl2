package org.example.shaders;


import org.example.entity.Camera;
import org.example.model.Light;
import org.example.model.Material;
import org.example.toolbox.Maths;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL20;

import java.util.List;

public class StaticShader extends ShaderProgram {

	private static final String VERTEX_FILE = "src/main/java/org/example/shaders/vertexShader.glsl";
	private static final String FRAGMENT_FILE = "src/main/java/org/example/shaders/fragmentShader.glsl";

	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_useColor;
	private int location_textureSampler1;
	private int location_cameraPos;
//	private int location_lightPosition;
//	private int location_lightAmbient;
//	private int location_lightDiffuse;
//	private int location_lightSpecular;
//	private int location_lightAttenuation;
	private int location_materialAmbient;
	private int location_materialDiffuse;
	private int location_materialSpecular;
	private int location_shininess;
	private int location_useBlinnPhong;
	private int location_useLighting;
	private int location_numLights;
	private int location_lightMode;
	private int location_lightIndex;
	private int[] location_lightPosition;
	private int[] location_lightAmbient;
	private int[] location_lightDiffuse;
	private int[] location_lightSpecular;
	private int[] location_lightAttenuation;
	private int location_useEmissive;
	private int location_isLightSource;


	public StaticShader() {
		super(VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "textureCoordinates");
//		super.bindAttribute(2, "vertexColor");
		super.bindAttribute(2, "normal");

	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = super.getUniformLocation("transformationMatrix");
		location_projectionMatrix = super.getUniformLocation("projectionMatrix");
		location_viewMatrix = super.getUniformLocation("viewMatrix");
		location_useColor = super.getUniformLocation("useColor");
		location_textureSampler1 = super.getUniformLocation("textureSampler1");

		location_cameraPos = super.getUniformLocation("cameraPos");
//		location_lightPosition = super.getUniformLocation("lightPosition");
//		location_lightAmbient = super.getUniformLocation("lightAmbient");
//		location_lightDiffuse = super.getUniformLocation("lightDiffuse");
//		location_lightSpecular = super.getUniformLocation("lightSpecular");
//		location_lightAttenuation = super.getUniformLocation("lightAttenuation");
		location_materialAmbient = super.getUniformLocation("materialAmbient");
		location_materialDiffuse = super.getUniformLocation("materialDiffuse");
		location_materialSpecular = super.getUniformLocation("materialSpecular");
		location_shininess = super.getUniformLocation("shininess");
		location_useBlinnPhong = super.getUniformLocation("useBlinnPhong");
		location_useLighting = super.getUniformLocation("useLighting");
		location_numLights = super.getUniformLocation("numLights");
		location_isLightSource = super.getUniformLocation("isLightSource");
		location_lightMode = super.getUniformLocation("lightMode");
		location_lightIndex = super.getUniformLocation("lightIndex");
		location_lightPosition = new int[4];
		location_lightAmbient = new int[4];
		location_lightDiffuse = new int[4];
		location_lightSpecular = new int[4];
		location_lightAttenuation = new int[4];


		for (int i = 0; i < 4; i++) {
			location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
			location_lightAmbient[i] = super.getUniformLocation("lightAmbient[" + i + "]");
			location_lightDiffuse[i] = super.getUniformLocation("lightDiffuse[" + i + "]");
			location_lightSpecular[i] = super.getUniformLocation("lightSpecular[" + i + "]");
			location_lightAttenuation[i] = super.getUniformLocation("lightAttenuation[" + i + "]");
		}
		location_useEmissive = super.getUniformLocation("useEmissive");


	}

	public void loadUseColor(boolean useColor) {
		super.loadBoolean(location_useColor, useColor);
	}

	public void loadTransformationMatrix(Matrix4f matrix) {
		super.loadMatrix(location_transformationMatrix, matrix);
	}

	public void loadViewMatrix(Camera camera) {
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		super.loadMatrix(location_viewMatrix, viewMatrix);
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		super.loadMatrix(location_projectionMatrix, projection);
	}

	public void loadTextureUnit(int textureUnit, int location) {
		super.loadInt(location, textureUnit);
	}
	public void loadUseEmissive(boolean useEmissive) {
		super.loadBoolean(location_useEmissive, useEmissive);
	}

	public void loadTextureUnits(int texture1) {
		loadTextureUnit(texture1, location_textureSampler1);
	}

	public void loadCameraPosition(Vector3f cameraPos) {
		super.loadVector(location_cameraPos, cameraPos);
	}

	public void loadLights(List<Light> lights) {
		for (int i = 0; i < lights.size(); i++) {
			super.loadVector(location_lightPosition[i], lights.get(i).getPosition());
			super.loadVector(location_lightAmbient[i], lights.get(i).getAmbient());
			super.loadVector(location_lightDiffuse[i], lights.get(i).getDiffuse());
			super.loadVector(location_lightSpecular[i], lights.get(i).getSpecular());
			super.loadVector(location_lightAttenuation[i], lights.get(i).getAttenuation());
		}
	}

	public void loadMaterial(Material material) {
		super.loadVector(location_materialAmbient, material.getAmbient());
		super.loadVector(location_materialDiffuse, material.getDiffuse());
		super.loadVector(location_materialSpecular, material.getSpecular());
		super.loadFloat(location_shininess, material.getShininess());
	}
	public void loadBoolean(String name, boolean value) {
		GL20.glUniform1i(getUniformLocation(name), value ? 1 : 0);
	}
	public void loadNumLights(int numLights) {
		super.loadInt(location_numLights, numLights);
	}
	public void loadLightMode(int lightMode) {
		super.loadInt(location_lightMode, lightMode);
	}
	public void loadLightIndex(int lightIndex) {
		super.loadInt(location_lightIndex, lightIndex);
	}
	public void loadIsLightSource(int isLightSource) {
		super.loadInt(location_isLightSource, isLightSource);
	}
}
