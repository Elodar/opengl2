package org.example.model;



public class RawModel {
	private int vaoId;
	private int vertexCount;
	private int uvBufferId; // ID dla bufora UV
	private float[] vertices;

	public float[] getVertices() {
		return vertices;
	}

	public void setVertices(float[] vertices) {
		this.vertices = vertices;
	}

	public RawModel(int vaoId, int vertexCount, int uvBufferId) {
		this.vaoId = vaoId;
		this.vertexCount = vertexCount;
		this.uvBufferId = uvBufferId;
	}
	public RawModel(int vaoId, int vertexCount, int uvBufferId, float[] vertices) {
		this.vaoId = vaoId;
		this.vertexCount = vertexCount;
		this.uvBufferId = uvBufferId;
		this.vertices = vertices;
	}

	public int getVaoId() {
		return vaoId;
	}

	public int getVertexCount() {
		return vertexCount;
	}

	public int getUvBufferId() {
		return uvBufferId;
	}
}