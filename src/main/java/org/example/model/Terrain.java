package org.example.model;

import org.example.renderEngine.Loader;
import org.joml.Vector3f;

public class Terrain {

    private RawModel model;
    private float[][] heights;
    private float gridX, gridZ;
    private float size; // Actual size of the terrain
    private int vertexCount;
    private float minX, maxX, minZ, maxZ;

    public Terrain(float gridX, float gridZ, Loader loader, RawModel model) {
        this.gridX = gridX;
        this.gridZ = gridZ;
        this.model = model;
        this.vertexCount = (int) Math.sqrt(model.getVertexCount() / 3); // Adjusting vertex count
        calculateTerrainBounds(model);
        this.heights = calculateHeights(model);
    }

    private void calculateTerrainBounds(RawModel model) {
        float[] vertices = model.getVertices();
        minX = Float.MAX_VALUE;
        maxX = Float.MIN_VALUE;
        minZ = Float.MAX_VALUE;
        maxZ = Float.MIN_VALUE;

        for (int i = 0; i < vertices.length; i += 3) {
            float x = vertices[i];
            float z = vertices[i + 2];
            if (x < minX) minX = x;
            if (x > maxX) maxX = x;
            if (z < minZ) minZ = z;
            if (z > maxZ) maxZ = z;
        }

        this.size = Math.max(maxX - minX, maxZ - minZ);
        System.out.printf("Terrain bounds: minX=%f, maxX=%f, minZ=%f, maxZ=%f%n", minX, maxX, minZ, maxZ);
    }

    private float[][] calculateHeights(RawModel model) {
        float[][] heights = new float[vertexCount][vertexCount];
        float[] vertices = model.getVertices();

        for (int i = 0; i < vertices.length; i += 3) {
            float x = vertices[i];
            float y = vertices[i + 1];
            float z = vertices[i + 2];
            int gridX = (int) ((x - minX) / (maxX - minX) * (vertexCount - 1));
            int gridZ = (int) ((z - minZ) / (maxZ - minZ) * (vertexCount - 1));

            if (gridX >= 0 && gridX < vertexCount && gridZ >= 0 && gridZ < vertexCount) {
                heights[gridX][gridZ] = y;
            }
        }
        return heights;
    }

    public float getHeight(float worldX, float worldZ) {
        float terrainX = (worldX - minX) / size * (vertexCount - 1);
        float terrainZ = (worldZ - minZ) / size * (vertexCount - 1);
        int gridX = (int) Math.floor(terrainX);
        int gridZ = (int) Math.floor(terrainZ);

        if (gridX < 0 || gridX >= vertexCount - 1 || gridZ < 0 || gridZ >= vertexCount - 1) {
            return 0;
        }

        float xCoord = terrainX - gridX;
        float zCoord = terrainZ - gridZ;

        float h00 = heights[gridX][gridZ];
        float h10 = heights[gridX + 1][gridZ];
        float h01 = heights[gridX][gridZ + 1];
        float h11 = heights[gridX + 1][gridZ + 1];

        float h0 = h00 * (1 - xCoord) + h10 * xCoord;
        float h1 = h01 * (1 - xCoord) + h11 * xCoord;

        float height = h0 * (1 - zCoord) + h1 * zCoord;
        System.out.printf("Height at (%f, %f): %f (gridX=%d, gridZ=%d, xCoord=%f, zCoord=%f)%n", worldX, worldZ, height, gridX, gridZ, xCoord, zCoord);

        return height;
    }

    public RawModel getModel() {
        return model;
    }

    public float getMinX() {
        return minX;
    }

    public float getMaxX() {
        return maxX;
    }

    public float getMinZ() {
        return minZ;
    }

    public float getMaxZ() {
        return maxZ;
    }
}